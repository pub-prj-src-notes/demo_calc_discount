## demo_calc_discount

Using
 * php 8.3.8

Tests
 * `./bin/phpunit`
 * `./vendor/phpunit/phpunit/phpunit`

Endpoint
 * GET `/api/v1/calcCostWithDiscount` request
