<?php

declare(strict_types=1);

namespace App\Domain;

class CalcDTO
{
    public PriceValue $basePrice;
    public DateValue $birthDate;
    public DateValue $startDate;
    public DateValue $paymentDate;

    public function __construct() {
        $this->basePrice = new PriceValue();
        $this->birthDate = new DateValue();
        $this->startDate = new DateValue();
        $this->paymentDate = new DateValue();
    }
}
