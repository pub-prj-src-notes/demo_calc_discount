<?php

declare(strict_types=1);

namespace App\Domain;

class DateValue
{
    public string $value;
}
