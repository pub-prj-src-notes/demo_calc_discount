<?php

declare(strict_types=1);

namespace App\Domain;

use App\Service\DateTimeService;
use DateTime;

class CalcDiscount
{
    private CalcDTO $calcDTO;
    private DateValue $currentDate;

    public function __construct(CalcDTO $calcDTO, string $currentDateString = '')
    {
        $this->calcDTO = $calcDTO;

        $this->currentDate = new DateValue();
        $this->currentDate->value = DateTimeService::createToDateValue($currentDateString);
    }

    public function applyPercentDiscount(PercentValue $percent): PriceValue
    {
        $discount = $this->calcDTO->basePrice->value * $percent->value / 100;

        if (($limit = $this->getLimitCostByPercent($percent)) !== 0) {
            $applyCostDiscount = ($discount > $limit) ? $limit : $discount;
        } else {
            $applyCostDiscount = $discount;
        }

        $priceWithDiscount = new PriceValue();
        $priceWithDiscount->value = $this->calcDTO->basePrice->value - $applyCostDiscount;
        return $priceWithDiscount;
    }

    private function getLimitCostByPercent(PercentValue $percentValue): int
    {
        $limitsInRuble = [
            30 => 4500,
            3 => 1500,
            5 => 1500,
            7 => 1500,
        ];

        return $limitsInRuble[$percentValue->value] ?? 0;
    }

    public function calc(): PercentValue
    {
        $calcPercent = $this->calcPercent();

        $percent = new PercentValue();
        $percent->value = $calcPercent;

        return $percent;
    }

    protected function calcPercent(): int
    {
        $startDateString = $this->calcDTO->startDate->value;
        $startDate = $this->strToDate($startDateString);

        $paidDate = null;
        if (isset($this->calcDTO->paymentDate->value)) {
            $paidDate = $this->strToDate($this->calcDTO->paymentDate->value);
        }

        $startYear = intval($startDate->format('Y'));
        $startMonth = intval($startDate->format('m'));
        $startDay = intval($startDate->format('d'));

        $paidYear = $paidDate ? intval($paidDate->format('Y')) : null;
        $paidMonth = $paidDate ? intval($paidDate->format('m')) : null;

        $currentYear = $this->getCurrentYear();
        $nextYear = $currentYear + 1;

        if ( // на путешествие с датой старта с 1 апреля по 30 сентября следующего года
            (4 <= $startMonth) and ($startMonth <= 9)
        ) {

            if (
                ($paidYear < $currentYear) and ($nextYear === $startYear)
            ) {
                return 7;
            }

            if (
                ($paidMonth <= 11) and ($currentYear === $paidYear) and ($nextYear === $startYear)
            ) {
                return 7;
            }

            if (
                (11 === $paidMonth) and ($currentYear === $paidYear)
            ) {
                return 7;
            }

            if (
                (12 === $paidMonth) and ($currentYear === $paidYear)
            ) {
                return 5;
            }

            if (
                (1 === $paidMonth) and ($currentYear === $paidYear) and ($startYear === $paidYear)
            ) {
                return 3;
            }
        }

        // на путешествия с датой старта от 1 октября текущего года по 14 января следующего года
        if (
            ((10 === $startMonth) and ($currentYear === $startYear))
            or
            ((11 === $startMonth) and ($currentYear === $startYear))
            or
            ((12 === $startMonth) and ($currentYear === $startYear))
            or
            ((1 === $startMonth) and (14 >= $startDay) and ($nextYear === $startYear))
        ) {

            if (
                ($currentYear > $paidYear)
            ) {
                return 7;
            }

            if (
                (11 >= $paidMonth) and ($currentYear === $paidYear)
            ) {
                return 7;
            }

            if (
                (11 === $paidMonth) and ($currentYear === $paidYear)
            ) {
                return 7;
            }

            if (
                (4 === $paidMonth) and ($currentYear === $paidYear)
            ) {
                return 5;
            }

            if (
                (5 === $paidMonth) and ($currentYear === $paidYear)
            ) {
                return 3;
            }
        }

        if ( //  на путешествия с датой старта с 15 января следующего года и далее
            ((1 === $startMonth) and (15 <= $startDay) and ($nextYear === $startYear))
            or
            ((1 < $startMonth) and ($nextYear === $startYear))
            or
            ($currentYear < $startYear)
        ) {

            if (
                ($currentYear > $paidYear)
            ) {
                return 7;
            }

            if (
                ($paidMonth < 8) and ($currentYear === $paidYear)
            ) {
                return 7;
            }

            if (
                (8 === $paidMonth) and ($currentYear === $paidYear)
            ) {
                return 7;
            }

            if (
                (9 === $paidMonth) and ($currentYear === $paidYear)
            ) {
                return 5;
            }

            if (
                (10 === $paidMonth) and ($currentYear === $paidYear)
            ) {
                return 3;
            }
        }

        // --
        if (!isset($this->calcDTO->birthDate->value)) {
            return 0;
        }

        $birthDate = $this->strToDate($this->calcDTO->birthDate->value);
        $countYears = $startDate->diff($birthDate)->y;

        if (
            (3 <= $countYears) and ($countYears <= 5)
        ) {
            return 80;
        }

        if (
            (6 <= $countYears) and ($countYears <= 11)
        ) {
            return 30;
        }

        if (
            (12 <= $countYears) and ($countYears <= 17)
        ) {
            return 10;
        }
        // --


        return 0;
    }

    private function strToDate(string $date): DateTime
    {
        return DateTimeService::from($date);
    }

    private function getCurrentYear(): int
    {
        return intval((new DateTime($this->currentDate->value))->format('Y'));
    }
}
