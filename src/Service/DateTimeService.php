<?php

namespace App\Service;

use DateTime;

class DateTimeService
{
    public static function from(string $dateTime): DateTime
    {
        return DateTime::createFromFormat('d.m.Y', $dateTime);
    }

    public static function createToDateValue(string $dateTime): string
    {
        return (new DateTime($dateTime))->format('d.m.Y');
    }
}
