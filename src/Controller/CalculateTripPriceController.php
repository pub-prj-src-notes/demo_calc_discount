<?php

namespace App\Controller;

use App\Domain\CalcDiscount;
use App\Domain\CalcDTO;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class CalculateTripPriceController extends AbstractController
{
    public function __construct(
        private CalcDiscount $calcDiscount,
        private CalcDTO $calcDTO
    ) {
    }

    #[Route('/api/v1/calcCostWithDiscount', name: 'app_calculate_trip_price', methods: ['GET'])]
    public function calcCostWithDiscount(
        Request $request
    ): Response {
        try {
            $basePrice = (int) $request->get('base');

            $birthDate = new \DateTime($request->get('birth'));

            $startValue = $request->get('start', '');
            $startDate = new \DateTime($startValue);

            $paymentValue = $request->get('payment', '');
            $paymentDate = $paymentValue ? new \DateTime($paymentValue) : null;

            $this->calcDTO->basePrice->value = $basePrice;
            $this->calcDTO->birthDate->value = $birthDate->format('d.m.Y');
            $this->calcDTO->startDate->value = $startDate->format('d.m.Y');
            if ($paymentDate) {
                $this->calcDTO->paymentDate->value = $paymentDate->format('d.m.Y');
            }

            $percentValue = $this->calcDiscount->calc();
            $priceValue = $this->calcDiscount->applyPercentDiscount($percentValue);

            $data = ['finalPrice' => $priceValue->value];

            return $this->json($data);
        } catch (\Exception $e) {
            return $this->json(['error' => $e->getMessage()], 400);
        }
    }
}
