<?php

declare(strict_types=1);

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class WebAPITestCaseTest extends WebTestCase
{
    public function testSomethingCalc(): void
    {
        $client = static::createClient();

        $client->request('GET', '/api/v1/calcCostWithDiscount?base=10000&birth=01.01.2020&start=01.01.2024');

        $this->assertResponseIsSuccessful();

        $response = $client->getResponse();

        $this->assertJsonStringEqualsJsonString(
            '{"finalPrice":2000}',
            $response->getContent()
        );
    }
}
