<?php

declare(strict_types=1);

namespace App\Tests;

use App\Domain\CalcDiscount;
use App\Domain\CalcDTO;
use PHPUnit\Framework\TestCase;

class CalcChildrensDiscount6YearsOldWithDiscountLimitTest extends TestCase
{
    public function testCalcDiscount_Case_when_from_6_years_old(): void
    {
        $calcDTO = new CalcDTO();
        $calcDTO->startDate->value = '01.01.2024';
        $calcDTO->paymentDate->value = '02.02.2024';

        $calcDTO->birthDate->value = '01.01.2017';

        $calcDTO->basePrice->value = 20_000;

        $expected = 30;

        $discountCalc = new CalcDiscount($calcDTO, '03.03.2024');
        $discountPercent = $discountCalc->calc();

        $this->assertNotNull($discountPercent->value);
        $this->assertSame($expected, $discountPercent->value);

        $discountCost = $discountCalc->applyPercentDiscount($discountPercent);
        $expectedCost = 15_500;

        $this->assertNotNull($discountCost->value);
        $this->assertSame($expectedCost, $discountCost->value);
    }

    public function testCalcDiscount_Case_when_from_6_years_old_exacly_apply_limit(): void
    {
        $calcDTO = new CalcDTO();
        $calcDTO->startDate->value = '01.01.2024';
        $calcDTO->paymentDate->value = '02.02.2024';

        $calcDTO->birthDate->value = '01.01.2017';

        $calcDTO->basePrice->value = 15_000;

        $expected = 30;

        $discountCalc = new CalcDiscount($calcDTO, '03.03.2024');
        $discountPercent = $discountCalc->calc();

        $this->assertNotNull($discountPercent->value);
        $this->assertSame($expected, $discountPercent->value);

        $discountCost = $discountCalc->applyPercentDiscount($discountPercent);
        $expectedCost = 10_500;

        $this->assertNotNull($discountCost->value);
        $this->assertSame($expectedCost, $discountCost->value);
    }

    public function testCalcDiscount_Case_when_from_6_years_old_without_apply_limit(): void
    {
        $calcDTO = new CalcDTO();
        $calcDTO->startDate->value = '01.01.2024';
        $calcDTO->paymentDate->value = '02.02.2024';

        $calcDTO->birthDate->value = '01.01.2017';

        $calcDTO->basePrice->value = 10_000;

        $expected = 30;

        $discountCalc = new CalcDiscount($calcDTO, '03.03.2024');
        $discountPercent = $discountCalc->calc();

        $this->assertNotNull($discountPercent->value);
        $this->assertSame($expected, $discountPercent->value);

        $discountCost = $discountCalc->applyPercentDiscount($discountPercent);
        $expectedCost = 7_000;

        $this->assertNotNull($discountCost->value);
        $this->assertSame($expectedCost, $discountCost->value);
    }
}
