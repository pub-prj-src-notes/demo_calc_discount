<?php

declare(strict_types=1);

namespace App\Tests;

use App\Domain\CalcDiscount;
use App\Domain\CalcDTO;
use PHPUnit\Framework\TestCase;

class CalcChildrensDiscountRuleTest extends TestCase
{
    public function testCalcDiscount_Case_when_from_3_years_old(): void
    {
        $calcDTO = new CalcDTO();
        $calcDTO->startDate->value = '01.01.2024';
        $calcDTO->paymentDate->value = '02.02.2024';

        $calcDTO->birthDate->value = '01.01.2020';

        $calcDTO->basePrice->value = 10_000;

        $expected = 80;

        $discountCalc = new CalcDiscount($calcDTO, '03.03.2024');
        $discountPercent = $discountCalc->calc();

        $this->assertNotNull($discountPercent->value);
        $this->assertSame($expected, $discountPercent->value);
    }

    public function testCalcDiscount_Case_when_from_6_years_old(): void
    {
        $calcDTO = new CalcDTO();
        $calcDTO->startDate->value = '01.01.2024';
        $calcDTO->paymentDate->value = '02.02.2024';

        $calcDTO->birthDate->value = '01.01.2017';

        $calcDTO->basePrice->value = 10_000;

        $expected = 30;

        $discountCalc = new CalcDiscount($calcDTO, '03.03.2024');
        $discountPercent = $discountCalc->calc();

        $this->assertNotNull($discountPercent->value);
        $this->assertSame($expected, $discountPercent->value);
    }

    public function testCalcDiscount_Case_when_from_12_years_old(): void
    {
        $calcDTO = new CalcDTO();
        $calcDTO->startDate->value = '01.01.2024';
        $calcDTO->paymentDate->value = '02.02.2024';

        $calcDTO->birthDate->value = '01.01.2011';

        $calcDTO->basePrice->value = 10_000;

        $expected = 10;

        $discountCalc = new CalcDiscount($calcDTO, '03.03.2024');
        $discountPercent = $discountCalc->calc();

        $this->assertNotNull($discountPercent->value);
        $this->assertSame($expected, $discountPercent->value);
    }
}
