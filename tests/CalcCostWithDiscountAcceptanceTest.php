<?php

declare(strict_types=1);

namespace App\Tests;

use App\Domain\CalcDiscount;
use App\Domain\CalcDTO;
use PHPUnit\Framework\TestCase;

class CalcCostWithDiscountAcceptanceTest extends TestCase
{
    public function testCalcDiscount_Case_01_00(): void
    {
        $calcDTO = new CalcDTO();
        $calcDTO->startDate->value = '01.01.2024';
        //$calcDTO->paymentDate->value = '25.10.2026';
        $calcDTO->birthDate->value = '01.01.2020';
        $calcDTO->basePrice->value = 10_000;
        $expectedPercent = 80;

        $discountCalc = new CalcDiscount($calcDTO, '30.11.2026');
        $discountPercent = $discountCalc->calc();

        $this->assertNotNull($discountPercent->value);
        $this->assertSame($expectedPercent, $discountPercent->value);

        $discountCost = $discountCalc->applyPercentDiscount($discountPercent);
        $expectedCost = 2_000;

        $this->assertNotNull($discountCost->value);
        $this->assertSame($expectedCost, $discountCost->value);
    }
}
