<?php

declare(strict_types=1);

namespace App\Tests;

use App\Domain\CalcDiscount;
use App\Domain\CalcDTO;
use PHPUnit\Framework\TestCase;

class CalcDiscountAcceptanceExampleTest extends TestCase
{
    public function testCalcDiscount_Case_01_00(): void
    {
        $calcDTO = new CalcDTO();
        $calcDTO->startDate->value = '01.05.2027';
        $calcDTO->paymentDate->value = '25.10.2026';
        $expected = 7;

        $discountCalc = new CalcDiscount($calcDTO, '30.11.2026');
        $discountPercent = $discountCalc->calc();

        $this->assertNotNull($discountPercent->value);
        $this->assertSame($expected, $discountPercent->value);
    }

    public function testCalcDiscount_Case_01_01(): void
    {
        $calcDTO = new CalcDTO();
        $calcDTO->startDate->value = '01.05.2027';
        $calcDTO->paymentDate->value = '29.11.2026';
        $expected = 7;

        $discountCalc = new CalcDiscount($calcDTO, '30.11.2026');
        $discountPercent = $discountCalc->calc();

        $this->assertNotNull($discountPercent->value);
        $this->assertSame($expected, $discountPercent->value);
    }

    public function testCalcDiscount_Case_01_02(): void
    {
        $calcDTO = new CalcDTO();
        $calcDTO->startDate->value = '01.05.2027';
        $calcDTO->paymentDate->value = '30.12.2026';
        $expected = 5;

        $discountCalc = new CalcDiscount($calcDTO, '31.12.2026');
        $discountPercent = $discountCalc->calc();

        $this->assertNotNull($discountPercent->value);
        $this->assertSame($expected, $discountPercent->value);
    }

    public function testCalcDiscount_Case_01_03(): void
    {
        $calcDTO = new CalcDTO();
        $calcDTO->startDate->value = '01.05.2027';
        $calcDTO->paymentDate->value = '30.01.2027';
        $expected = 3;

        $discountCalc = new CalcDiscount($calcDTO, '31.01.2027');
        $discountPercent = $discountCalc->calc();

        $this->assertNotNull($discountPercent->value);
        $this->assertSame($expected, $discountPercent->value);
    }

    public function testCalcDiscount_Case_02_00(): void
    {
        $calcDTO = new CalcDTO();
        $calcDTO->startDate->value = '15.01.2027';
        $calcDTO->paymentDate->value = '20.07.2026';
        $expected = 7;

        $discountCalc = new CalcDiscount($calcDTO, '31.08.2026');
        $discountPercent = $discountCalc->calc();

        $this->assertNotNull($discountPercent->value);
        $this->assertSame($expected, $discountPercent->value);
    }

    public function testCalcDiscount_Case_02_01(): void
    {
        $calcDTO = new CalcDTO();
        $calcDTO->startDate->value = '15.01.2027';
        $calcDTO->paymentDate->value = '30.08.2026';
        $expected = 7;

        $discountCalc = new CalcDiscount($calcDTO, '31.08.2026');
        $discountPercent = $discountCalc->calc();

        $this->assertNotNull($discountPercent->value);
        $this->assertSame($expected, $discountPercent->value);
    }

    public function testCalcDiscount_Case_02_02(): void
    {
        $calcDTO = new CalcDTO();
        $calcDTO->startDate->value = '15.01.2027';
        $calcDTO->paymentDate->value = '29.09.2026';
        $expected = 5;

        $discountCalc = new CalcDiscount($calcDTO, '30.09.2026'); // DateValue $paidBeforeDae
        $discountPercent = $discountCalc->calc();

        $this->assertNotNull($discountPercent->value);
        $this->assertSame($expected, $discountPercent->value);
    }

    public function testCalcDiscount_Case_02_03(): void
    {
        $calcDTO = new CalcDTO();
        $calcDTO->startDate->value = '15.01.2027';
        $calcDTO->paymentDate->value = '30.10.2026';
        $expected = 3;

        $discountCalc = new CalcDiscount($calcDTO, '31.10.2026');
        $discountPercent = $discountCalc->calc();

        $this->assertNotNull($discountPercent->value);
        $this->assertSame($expected, $discountPercent->value);
    }
}
