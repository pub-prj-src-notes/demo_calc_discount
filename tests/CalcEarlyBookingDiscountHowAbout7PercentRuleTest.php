<?php

declare(strict_types=1);

namespace App\Tests;

use App\Domain\CalcDiscount;
use App\Domain\CalcDTO;
use PHPUnit\Framework\TestCase;

class CalcEarlyBookingDiscountHowAbout7PercentRuleTest extends TestCase
{
    // @todo add от 3% до 7%, но не более 1500
    public function testCalcDiscount_Case_01_april(): void
    {
        $calcDTO = new CalcDTO();
        $calcDTO->startDate->value = '01.04.2027';
        $calcDTO->paymentDate->value = '25.11.2026';
        $expected = 7;

        $discountCalc = new CalcDiscount($calcDTO, '30.11.2026');
        $discountPercent = $discountCalc->calc();

        $this->assertNotNull($discountPercent->value);
        $this->assertSame($expected, $discountPercent->value);
    }

    public function testCalcDiscount_Case_01_may(): void
    {
        $calcDTO = new CalcDTO();
        $calcDTO->startDate->value = '01.05.2027';
        $calcDTO->paymentDate->value = '25.11.2026';
        $expected = 7;

        $discountCalc = new CalcDiscount($calcDTO, '30.11.2026');
        $discountPercent = $discountCalc->calc();

        $this->assertNotNull($discountPercent->value);
        $this->assertSame($expected, $discountPercent->value);
    }

    public function testCalcDiscount_Case_01_iun(): void
    {
        $calcDTO = new CalcDTO();
        $calcDTO->startDate->value = '01.06.2027';
        $calcDTO->paymentDate->value = '25.11.2026';
        $expected = 7;

        $discountCalc = new CalcDiscount($calcDTO, '30.11.2026');
        $discountPercent = $discountCalc->calc();

        $this->assertNotNull($discountPercent->value);
        $this->assertSame($expected, $discountPercent->value);
    }

    public function testCalcDiscount_Case_01_iul(): void
    {
        $calcDTO = new CalcDTO();
        $calcDTO->startDate->value = '01.07.2027';
        $calcDTO->paymentDate->value = '25.11.2026';
        $expected = 7;

        $discountCalc = new CalcDiscount($calcDTO, '30.11.2026');
        $discountPercent = $discountCalc->calc();

        $this->assertNotNull($discountPercent->value);
        $this->assertSame($expected, $discountPercent->value);
    }

    public function testCalcDiscount_Case_01_august(): void
    {
        $calcDTO = new CalcDTO();
        $calcDTO->startDate->value = '01.08.2027';
        $calcDTO->paymentDate->value = '25.11.2026';
        $expected = 7;

        $discountCalc = new CalcDiscount($calcDTO, '30.11.2026');
        $discountPercent = $discountCalc->calc();

        $this->assertNotNull($discountPercent->value);
        $this->assertSame($expected, $discountPercent->value);
    }

    public function testCalcDiscount_Case_01_sent_01(): void
    {
        $calcDTO = new CalcDTO();
        $calcDTO->startDate->value = '01.09.2027';
        $calcDTO->paymentDate->value = '25.11.2026';
        $expected = 7;

        $discountCalc = new CalcDiscount($calcDTO, '30.11.2026');
        $discountPercent = $discountCalc->calc();

        $this->assertNotNull($discountPercent->value);
        $this->assertSame($expected, $discountPercent->value);
    }

    public function testCalcDiscount_Case_01_sent_30(): void
    {
        $calcDTO = new CalcDTO();
        $calcDTO->startDate->value = '30.09.2027';
        $calcDTO->paymentDate->value = '25.11.2026';
        $expected = 7;

        $discountCalc = new CalcDiscount($calcDTO, '30.11.2026');
        $discountPercent = $discountCalc->calc();

        $this->assertNotNull($discountPercent->value);
        $this->assertSame($expected, $discountPercent->value);
    }


    public function testCalcDiscount_Case_02_okt(): void
    {
        $calcDTO = new CalcDTO();
        $calcDTO->startDate->value = '01.10.2027';
        $calcDTO->paymentDate->value = '25.03.2027';
        $expected = 7;

        $discountCalc = new CalcDiscount($calcDTO, '24.03.2027');
        $discountPercent = $discountCalc->calc();

        $this->assertNotNull($discountPercent->value);
        $this->assertSame($expected, $discountPercent->value);
    }

    public function testCalcDiscount_Case_02_november(): void
    {
        $calcDTO = new CalcDTO();
        $calcDTO->startDate->value = '01.11.2027';
        $calcDTO->paymentDate->value = '25.03.2027';
        $expected = 7;

        $discountCalc = new CalcDiscount($calcDTO, '24.03.2027');
        $discountPercent = $discountCalc->calc();

        $this->assertNotNull($discountPercent->value);
        $this->assertSame($expected, $discountPercent->value);
    }

    public function testCalcDiscount_Case_02_december(): void
    {
        $calcDTO = new CalcDTO();
        $calcDTO->startDate->value = '01.12.2027';
        $calcDTO->paymentDate->value = '25.03.2027';
        $expected = 7;

        $discountCalc = new CalcDiscount($calcDTO, '24.03.2027');
        $discountPercent = $discountCalc->calc();

        $this->assertNotNull($discountPercent->value);
        $this->assertSame($expected, $discountPercent->value);
    }

    public function testCalcDiscount_Case_02__14_of_january_next_year(): void
    {
        $calcDTO = new CalcDTO();
        $calcDTO->startDate->value = '13.01.2027';
        $calcDTO->paymentDate->value = '25.03.2026';
        $expected = 7;

        $discountCalc = new CalcDiscount($calcDTO, '30.11.2026');
        $discountPercent = $discountCalc->calc();

        $this->assertNotNull($discountPercent->value);
        $this->assertSame($expected, $discountPercent->value);
    }

    public function testCalcDiscount_Case_03__15_of_january_next_year(): void
    {
        $calcDTO = new CalcDTO();
        $calcDTO->startDate->value = '15.01.2027';
        $calcDTO->paymentDate->value = '25.08.2026';
        $expected = 7;

        $discountCalc = new CalcDiscount($calcDTO, '30.11.2026');
        $discountPercent = $discountCalc->calc();

        $this->assertNotNull($discountPercent->value);
        $this->assertSame($expected, $discountPercent->value);
    }

    public function testCalcDiscount_Case_03__since_january_next_year_and_later(): void
    {
        $calcDTO = new CalcDTO();
        $calcDTO->startDate->value = '01.02.2027';
        $calcDTO->paymentDate->value = '25.08.2026';
        $expected = 7;

        $discountCalc = new CalcDiscount($calcDTO, '30.11.2026');
        $discountPercent = $discountCalc->calc();

        $this->assertNotNull($discountPercent->value);
        $this->assertSame($expected, $discountPercent->value);
    }
}
